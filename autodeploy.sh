#!/bin/bash
current_time=`date '+%Y-%m-%d-%H-%M-%S'`
code_dir=/data/deploy
deploy_dir=/data/codes/frontend
if [ ! -d ${code_dir} ];then
	mkdir ${code_dir}
fi

tar zxf /root/bcp.tar.gz -C ${code_dir} && rm bcp.tar.gz
mv ${code_dir}/bcp ${code_dir}/bcp-${current_time}
if [ -d ${deploy_dir}/bcp ];then
	/usr/bin/rm -rf ${deploy_dir}/bcp
fi
ln -sv ${code_dir}/bcp-${current_time} ${deploy_dir}/bcp
chown nginx.nginx -R ${deploy_dir}/bcp
