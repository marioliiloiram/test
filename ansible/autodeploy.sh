#!/bin/bash
current_time=`date '+%Y-%m-%d-%H-%M-%S'`
code_dir=/data/deploy
deploy_dir=/data/codes/frontend
if [ ! -d ${code_dir} ];then
	mkdir -p ${code_dir}
fi

if [ ! -d ${deploy_dir} ];then
        mkdir -p ${deploy_dir}
fi

tar zxf /root/bcp.tar.gz -C ${code_dir} 
mv ${code_dir}/bcp ${code_dir}/bcp-${current_time}
ls /data/codes/frontend/|grep bcp
if [ $? == 0 ];then
	/usr/bin/rm -rf ${deploy_dir}/bcp
fi
ln -sv ${code_dir}/bcp-${current_time} ${deploy_dir}/bcp
chown -R nginx.nginx ${deploy_dir}/bcp
