Date.prototype.Format = function(fmt) { //author: meizz
    var o = {
        "M+": this.getMonth() + 1, //月份
        "d+": this.getDate(), //日
        "h+": this.getHours(), //小时
        "m+": this.getMinutes(), //分
        "s+": this.getSeconds(), //秒
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度
        "S": this.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
let formatDate = value => {
    if (!value || value == "0001-01-01T00:00:00Z") return "--";
    value = new Date(value).Format("yyyy-MM-dd hh:mm:ss")
    return value
}
let money = (Odds, x) => {
    if (isNaN(Odds)) {
        return "0.00"
    }
    if (!x) {
        x = 2
    }
    return Number(Odds).toFixed(x)
}
let recordLength = function (result, key) {
    this.$set(key, result.length);
    return result;
}

export { formatDate, money ,recordLength}