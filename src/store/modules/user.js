import api from '@/api'
import router from '@/router'
import aes from 'crypto-js/aes'
import enc from 'crypto-js/enc-utf8'
import md5 from 'crypto-js/md5'

const state = {
    user: { name: "游客" },
    modalBox: "",
    token: null,
    centreBox: "centre",
    snackbar: { show: false, color: '', text: '' },
    order: {},
    auth: true,
    balance: {},
    loadBalance: false,
    ping: null,
    fullscreen:false,
    remeber:{},
    salt:""
}

const actions = {
    onLoad: ({ dispatch,commit }) => {
        let salt = localStorage.getItem("__t")
        if (!salt){salt = md5("T" + new Date().getTime()).toString()}
        commit("Salt",salt)
        dispatch("CONFIGURE")
        dispatch("MENU")
        dispatch("CheckUserLogin")
    },
    CheckUserLogin: ({ commit, dispatch }) => {
        var token = window.localStorage.getItem('token')
        var user = window.localStorage.getItem('user')
        if (user != null && token != null) {
            try {
                user = JSON.parse(aes.decrypt(user, md5(token).toString()).toString(enc))
            } catch (e) {
                commit('LOGOUT')
                router.push("/")
                return
            }
            user['token'] = token
            commit('onUserLogin', user)
            dispatch("BALANCE")
            dispatch("PING")
        }
    },
    async ['BALANCE']({ commit }) {
        if (state.loadBalance == false) {
            commit('BalanceLoading', true)
            await api.get("/pc/usercentre/balance/all").then(data => {
                var balance = {}
                for (var i in data.result) {
                    balance[i] = { "name": data.map[i], "balance": data.result[i] }
                }
                state.balance = balance
                setTimeout(function () { commit('BalanceLoading', false) }, 3000)
            }).catch(error => {
                commit("MsgError", error.error)
                setTimeout(function () { commit('BalanceLoading', false) }, 3000)
            })
        }
    },
    async ['ORDER']({ commit }, id) {
        await api.get("/pc/usercentre/deposit/" + id).then(data => {
            commit("SetOrder", data.result)
            commit("ShowGlobal", 'order')
        }).catch(error => {
            commit("MsgError", error.error)
        })
    },
    async ['PING']({ commit }, id) {
        if (state.ping != null) return
        var timer = setInterval(function () {
            if (state.token) api.get("/pc/usercentre/ping").catch(err => {
                if (err.error) {
                    commit("MsgError", err.error)
                }
            })
        }, 10000)
        commit("Ping", timer)
    },
    ShowGlobal({ commit }, b) {
        commit('modalBox', b)
    },
    ShowCentre: ({ commit }, b) => {
        commit('centreBox', b)
    },
    Remeber:({commit}) => {
        let remeber = window.localStorage.getItem('__r')
        let salt = window.localStorage.getItem('__t')
        if (remeber){
            try {
                let b = JSON.parse(aes.decrypt(remeber, salt+salt).toString(enc))
                commit('Remeber', b)
            } catch (e) {
                return
            }
        }
    }
}
const mutations = {
    ['LOGOUT'](state) {
        state.token = ''
        if (typeof window !== 'undefined') {
            // localStorage.clear()
            localStorage.removeItem("token")
            localStorage.removeItem("user")

        }
        setTimeout(function(){ router.push("/") },3000)
    },
    onUserLogin(state, user) {
        let token = user.token
        state.token = token
        localStorage.setItem('token', token)
        state.user = user
        localStorage.setItem('user', aes.encrypt(JSON.stringify(user), md5(token).toString()).toString())
    },
    ShowGlobal(state, b) {
        state.modalBox = b
    },
    ShowCentre(state, b) {
        localStorage.setItem('centreBox', b)
        state.centreBox = b
    },
    SetOrder(state, b) {
        b.postscript = b.order.slice(-4)
        state.order = b
    },
    MsgOk(state, b) {
        state.snackbar = { show: true, color: 'success', text: b }
    },
    MsgError(state, b) {
        state.snackbar = { show: true, color: 'error', text: b }
    },
    Forbidden(state) {
        state.auth = false
    },
    BalanceLoading(state, b) {
        state.loadBalance = b
    },
    Ping(state, b) {
        state.ping = b
    },
    Fullscreen(state,b) {
        state.fullscreen = b
    },
    Remeber(state,b) {
        state.remeber = b
        localStorage.setItem("__r",aes.encrypt(JSON.stringify(b), state.salt + state.salt).toString())
    },
    Salt(state,b) {
        state.salt = b
        localStorage.setItem("__t",b)
    }
}

const getters = {
    ['user'](state) {
        return state.user
    },
    ['token'](state) {
        return state.token
    },
    ['modalBox'](state) {
        return state.modalBox
    },
    ['centreBox'](state) {
        return state.centreBox
    },
    ['snackbar'](state) {
        return state.snackbar
    },
    ['order'](state) {
        return state.order
    },
    ['auth'](state) {
        return state.auth
    },
    ['loadBalance'](state) {
        return state.loadBalance
    },
    ['balance'](state) {
        return state.balance
    },
    ['fullscreen'](state) {
        return state.fullscreen
    },
    ['remeber'](state) {
        return state.remeber
    }
}

export default {
    actions,
    state,
    mutations,
    getters
}